package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;


import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {

	String name;
		
	List<String> link = null;
	
	@XmlAttribute
	int id;
	
	String description;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public List<String> getLink() {
        return link;
    }
 
    public void setLink(List<String> link) {
        this.link = link;
    }
    
    public int getProjectId(){
    	return id;
    }
    
    public void setProjectId(int projectId){
    	this.id= projectId;
    }


	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription(){
		return description;
	}
}
