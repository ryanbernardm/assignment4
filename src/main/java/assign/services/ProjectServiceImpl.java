package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.Course;
import assign.domain.NewCourse;
import assign.domain.Project;

public class ProjectServiceImpl implements ProjectService {

	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;

	// DB connection information would typically be read from a config file.
	public ProjectServiceImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUsername(this.dbUsername);
        ds.setPassword(this.dbPassword);
        ds.setUrl(this.dbURL);
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        return ds;
    }

	@Override
	public Project getProject(int projectId) throws Exception {
		String query = "select * from projects where project_id=" + projectId;
		Connection conn = ds.getConnection();
		PreparedStatement s = conn.prepareStatement(query);
		ResultSet r = s.executeQuery();
		
		if (!r.next()) {
			return null;
		}
		
		Project c = new Project();
		c.setDescription(r.getString("description"));
		c.setName(r.getString("name"));
		c.setProjectId(r.getInt("project_id"));
		return c;
	}

	@Override
	public Project addProject(Project newProject) throws SQLException {
		Connection conn = ds.getConnection();
		String insert = "INSERT INTO projects(name,description) values(?,?)";
		PreparedStatement stmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, newProject.getName());
		stmt.setString(2, newProject.getDescription());
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating project failed, no rows affected.");
        }
        
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
        	newProject.setProjectId(generatedKeys.getInt(1));
        }
        else {
            throw new SQLException("Creating project failed, no ID obtained.");
        }
        // Close the connection
        conn.close();
		return newProject;
	}
	
	public void updateProjectDesc(Project projectWithNewDescription) throws SQLException{
		Connection conn = ds.getConnection();
		
		String query = "UPDATE projects SET description=? where project_id=?";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, projectWithNewDescription.getDescription());
		stmt.setInt(2, projectWithNewDescription.getProjectId());
		stmt.executeUpdate();
		stmt.close();
		
		//close
		conn.close();
	}
	
	public void deleteProject(Integer project_id) throws SQLException{
		Connection conn = ds.getConnection();
		
		String query = "DELETE FROM projects WHERE project_id=?";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		
		stmt.setInt(1, project_id);
		stmt.executeUpdate();
		stmt.close();
		
		//close connection
		conn.close();
	}
}
