package assign.services;

import java.sql.SQLException;

import assign.domain.Course;
import assign.domain.NewCourse;
import assign.domain.Project;

public interface ProjectService {
	
	public Project getProject(int projectId) throws Exception;

	public Project addProject(Project newProject) throws SQLException;
	
	public void updateProjectDesc(Project existingProject) throws SQLException;

	public void deleteProject(Integer projectId) throws SQLException;

}
