package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import assign.domain.Course;
import assign.domain.Courses;
import assign.domain.NewCourse;
import assign.domain.NotFound;
import assign.domain.Project;
import assign.domain.Projects;
import assign.services.ProjectService;
import assign.services.ProjectServiceImpl;

@Path("/projects")
public class UTCoursesResource {
	
	ProjectService projectService;
	String password;
	String username;
	String dburl;	
	String dbhost, dbname;
	public UTCoursesResource(@Context ServletContext servletContext) {
		dbhost = servletContext.getInitParameter("DBHOST");
		dbname = servletContext.getInitParameter("DBNAME");
		//dburl = servletContext.getInitParameter("DBURL");
		dburl = "jdbc:mysql://" + dbhost + ":3306/" + dbname;
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.projectService = new ProjectServiceImpl(dburl, username, password);
	}
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password;		
	}
	
	@POST
	@Path("/")
	@Consumes("application/xml")
	public Response createProject(InputStream is) throws SQLException {
	      Project newProject = readNewProject(is);
	      if(newProject.getName().trim().equals("")|| newProject.getDescription().trim().equals("")){
		        throw new WebApplicationException(Response.Status.BAD_REQUEST);
	      }
	      newProject = this.projectService.addProject(newProject);
	      return Response.created(URI.create("/myeavesdrop/projects" + newProject.getProjectId())).build();
	}
		
	@PUT
	@Path("/{project_id}")
	@Consumes("application/xml")
	public void updateProj(@PathParam("project_id") Integer project_id, InputStream is) throws Exception{
		Project newProject = readNewProject(is);
		Project temp = projectService.getProject(project_id);
		if(temp == null){
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		if(newProject.getName().trim().equals("") || newProject.getDescription().trim().equals("")){
		    throw new WebApplicationException(Response.Status.BAD_REQUEST);
	    }
		projectService.updateProjectDesc(newProject);
	}
	
	@GET
	@Path("/{project_id}")
	@Produces("application/xml")
	public StreamingOutput getProject(@PathParam("project_id") Integer project_id, InputStream is) throws Exception {
		final Project temp = projectService.getProject(project_id);
		if(temp == null){
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		  return new StreamingOutput() {
		         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
		            outputProject(outputStream, temp);
		         }
		      };
	}
	
	@DELETE
	@Path("/{project_id}")
	public void deleteProj(@PathParam("project_id") Integer id) throws Exception{
		if(projectService.getProject(id) == null){
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		projectService.deleteProject(id);
	}
	
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected Project readNewProject(InputStream is) {
		      try {
		         DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		         Document doc = builder.parse(is);
		         Element root = doc.getDocumentElement();
		         Project project = new Project();
		         NodeList nodes = root.getChildNodes();
		         for (int i = 0; i < nodes.getLength(); i++) {
		            Element element = (Element) nodes.item(i);
		            if (element.getTagName().equals("name")) {
		               project.setName(element.getTextContent());
		            }
		            else if (element.getTagName().equals("description")) {
		               project.setDescription(element.getTextContent());
		            }
		         }
		         return project;
		      }
		      catch (Exception e) {
		         throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		      }
		   }
}